#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>

struct Tovar
{
	char name[20];
	int price;
	int quantity;
	int percent;
};
struct Table
{
	struct Tovar el[20];
	int n;
	
};
void printConsole(struct Table *T)
{
	for (int d = 0; d < T->n; d++)
	{
		printf("%-20s", T->el[d].name);
		printf("%-7d ", T->el[d].price);
		printf("%-3d",  T->el[d].quantity);
		printf("%-3d",  T->el[d].percent);
		printf("\n");
	}
};
void savetoFile(struct Table* T)
{
	FILE* fp;
		fp = fopen("WORK1.txt", "w");
		for (int d = 0; d < T->n; d++)
		{	
			fprintf(fp, "%s ", T->el[d].name);
			fprintf(fp, "%d ", T->el[d].price);
			fprintf(fp, "%d ", T->el[d].quantity); 
			fprintf(fp, "%d\n", T->el[d].percent);	
			
		}
		fclose(fp);
}
int put(struct Table* T,struct Tovar* x)
{
	if (T->n < 20)
	{
		T->el[T->n] = *x;
		T->n++;
		return 0;
	}
	else return 1;
}
void loadfromFile(struct Table* T)
{
	FILE* fp;
	struct Tovar x;
	
	int f = 0;
	fp = fopen("WORK.txt", "r+");
	while(!feof(fp)&&f==0)
	{
		fscanf(fp, "%s", &x.name);
		fscanf(fp, "%d", &x.price);
		fscanf(fp, "%d", &x.quantity);
		fscanf(fp, "%d", &x.percent);
		f=put(T, &x);
	}
	fclose(fp);
}
void Swap(struct Table* T, int left, int right)
{
	struct Tovar elem1,  elem2;
	elem1 = T->el[left];
	elem2 = T->el[right];
	T->el[left] = elem2;
	T->el[right] = elem1;

}
void Sort(struct Table* T, int field)
{
	int flag;
	switch (field)
	{
	case(1):
		do
		{
			flag = 0;
			for (int i = 1; i < T->n; i++)
			{
				if (strcmp(T->el[i - 1].name, T->el[i].name) > 0)
				{
					Swap(T, i - 1, i);
					flag = 1;
				}
			}
		} while (flag);
		break;
	case(2):
		do
		{
			flag = 0;
			for (int i = 1; i < T->n; i++)
			{
				if (T->el[i - 1].price> T->el[i].price)
				{
					Swap(T, i - 1, i);
					flag = 1;
				}
			}
		} while (flag); 
		break;
	case(3):
		do
		{
			flag = 0;
			for (int i = 1; i < T->n; i++)
			{
				if (T->el[i - 1].quantity > T->el[i].quantity)
				{
					Swap(T, i - 1, i);
					flag = 1;
				}
			}
		} while (flag);
		break;
	case(4):
		do
		{
			flag = 0;
			for (int i = 1; i < T->n; i++)
			{
				if (T->el[i - 1].percent > T->el[i].percent)
				{
					Swap(T, i - 1, i);
					flag = 1;
				}
			}
		} while (flag); 
		break;
	default:break;
	}
}
void Del(struct Table* T,int num)
{
	for (int i = num; i < T->n-1; i++)
	{
		T->el[i] = T->el[i + 1];
	}
	T->n--;
}
void Search(struct Table* T,int field,char value[])
{
	int flag = 0;
	switch (field)
	{
	case(1):
		for (int i = 0; i < T->n; i++)
		{
			if (strcmp(T->el[i].name, value) == 0)
			{
				printf("%-20s", T->el[i].name);
				printf("%-7d ", T->el[i].price);
				printf("%-3d", T->el[i].quantity);
				printf("%-3d", T->el[i].percent);
				printf("\n");
				flag++;
			}
		}
		   break;
	case(2):
		for (int i = 0; i < T->n; i++)
		{
			if (T->el[i].price==atoi(value))
			{
				printf("%-20s", T->el[i].name);
				printf("%-7d ", T->el[i].price);
				printf("%-3d", T->el[i].quantity);
				printf("%-3d", T->el[i].percent);
				printf("\n");
				flag++;
			}
		}
		break;
	case(3):
		for (int i = 0; i < T->n; i++)
		{
			if (T->el[i].quantity == value - '0')
			{
				printf("%-20s", T->el[i].name);
				printf("%-7d ", T->el[i].price);
				printf("%-3d", T->el[i].quantity);
				printf("%-3d", T->el[i].percent);
				printf("\n");
				flag++;
			}
		}
		break;
	case(4):
		for (int i = 0; i < T->n; i++)
		{
			if (T->el[i].percent == value - '0')
			{
				printf("%-20s", T->el[i].name);
				printf("%-7d ", T->el[i].price);
				printf("%-3d", T->el[i].quantity);
				printf("%-3d", T->el[i].percent);
				printf("\n");
				flag++;
			}
		}
		break;
	default:
		break;
	}
	if (flag == 0) printf("No matches");
	
}
void Change(struct Table* T)
{

}
void kbrdInsert(struct Table* T)
{
	struct Tovar elem;
	printf("\nName:");
	scanf("%s", &elem.name);
	printf("\nPrice:");
	scanf("%d", &elem.price);
	printf("\nQuantity:");
	scanf("%d", &elem.quantity);
	printf("\nPercent");
	scanf("%d", &elem.percent);
	if (put(T, &elem) == 0) printf("\nSuccess");
	else printf("\nFail");
}
void main()
{
	struct Table T;
	T.n = 0;
	loadfromFile(&T);
	//Sort(&T, 2);
	printConsole(&T);
	//savetoFile(&T);
	//Search(&T, 2, "1231");
	//Del(&T, 2);
	//printConsole(&T);
	kbrdInsert(&T);
	getchar();
}